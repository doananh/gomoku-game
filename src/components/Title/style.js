import styled from 'styled-components';

const ComponentTitleWrapper = styled.h1`
  font-size: 19px;
  font-weight: 500;
  color: #788195;
  width: 100%;
  margin-bottom: 30px;
  display: flex;
  align-items: center;
  white-space: nowrap;

  @media only screen and (max-width: 767px) {
    margin: 0 10px;
    margin-bottom: 30px;
  }

  &:before {
    content: '';
    width: 5px;
    height: 40px;
    background-color: #e4e6e9;
    display: flex;
    margin: 0 15px 0 0;
  }

  &:after {
    content: '';
    width: 100%;
    height: 1px;
    background-color: #e4e6e9;
    display: flex;
    margin: 0 0 0 15px;
  }
`;

export default ComponentTitleWrapper;
