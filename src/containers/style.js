import styled from 'styled-components';

const HomeWrapper = styled.div`
  font-size: 14px;

  .ant-layout-header {
    background: white;
    border-bottom: #e9e9e9 solid 1px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;

    .title {
      font-size: 25px;
    }

    .user-info {
      margin-left: auto;
      display: flex;
      align-items: center;

      .name {
        margin-right: 10px;
      }
    }
  }

  .ant-layout-content {
    background: #50bcac;
  }

  .ant-layout-sider {
    border-left: #50bcac solid 1px;
    background: white;
  }
`;

export default HomeWrapper;
