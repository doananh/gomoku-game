import React from 'react';
import { Button } from 'antd';
import Title from '../../../components/Title';
import FindMatchSectionWrapper from './style';

const FindMatchSection = () => {
  return (
    <FindMatchSectionWrapper>
      <Title>Start new game</Title>
      <div className="find-match-div">
        <Button type="primary" size="large" className="find-match-btn">
          Find Match
        </Button>
      </div>
    </FindMatchSectionWrapper>
  );
};

export default FindMatchSection;
