import styled from 'styled-components';

const FindMatchSectionWrapper = styled.div`
  .find-match-div {
    text-align: center;

    .find-match-btn {
      width: 200px;
      padding: 0 15px;
      font-size: 16px;
      border-radius: 4px;
      height: 40px;
    }
  }
`;

export default FindMatchSectionWrapper;
