import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Button } from 'antd';
import { loginAction } from '../../redux/auth/actions';
import LoginWrapper from './style';

const Login = props => {
  return (
    <LoginWrapper>
      <div>
        <div className="title">Please login to match a game ^.^</div>
        <div>
          <Button type="primary" size="large" onClick={props.login}>
            Login with facebook
          </Button>
        </div>
      </div>
    </LoginWrapper>
  );
};

Login.propTypes = {
  login: PropTypes.func,
};

export default connect(
  () => {
    return {};
  },
  dispatch => {
    return {
      login: () => {
        dispatch(loginAction());
      },
    };
  },
)(Login);
