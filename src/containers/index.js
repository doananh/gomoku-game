import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Layout, Dropdown, Avatar, Menu } from 'antd';
import Login from './Login';
import Game from './Game';
import { getCurrentUserAction } from '../redux/auth/actions';
import HomeWrapper from './style';

const { Header, Sider, Content } = Layout;
const { Item } = Menu;

class Home extends Component {
  componentDidMount() {
    this.props.getCurrentUser();
  }
  render() {
    const profileMenu = (
      <Menu>
        <Item>
          <a role="button">Logout</a>
        </Item>
      </Menu>
    );
    return (
      <div>
        <HomeWrapper>
          <Layout style={{ height: '100vh' }}>
            <Header>
              <div className="title">Gomoku</div>
              {this.props.isAuthenticated && (
                <Dropdown overlay={profileMenu} trigger={['click']}>
                  <div className="user-info">
                    <span className="name">{this.props.currentUser.displayName}</span>
                    {this.props.currentUser.photoURL && (
                      <Avatar src={this.props.currentUser.photoURL} />
                    )}
                    {!this.props.currentUser.photoURL && <Avatar icon="user" />}
                  </div>
                </Dropdown>
              )}
            </Header>
            <Layout>
              <Content>
                <Game />
              </Content>
              <Sider width={320}>{!this.props.isAuthenticated && <Login />}</Sider>
            </Layout>
          </Layout>
        </HomeWrapper>
      </div>
    );
  }
}

Home.propTypes = {
  getCurrentUser: PropTypes.func,
  isAuthenticated: PropTypes.bool,
  currentUser: PropTypes.object,
};

export default connect(
  state => {
    return {
      isAuthenticated: state.auth.isAuthenticated,
      currentUser: state.auth.currentUser,
    };
  },
  dispatch => {
    return {
      getCurrentUser: () => {
        dispatch(getCurrentUserAction());
      },
    };
  },
)(Home);
