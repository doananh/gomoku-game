import styled from 'styled-components';

const GameWrapper = styled.div`
  height: 100%;

  & > div {
    height: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: row;
    flex-flow: wrap;
  }

  .btn---board {
    position: relative;
    border: 1px solid #4abdac;
    background-color: rgba(255, 255, 255, 0.2);
    padding: 0;
  }

  .btn---board:hover {
    background-color: rgba(255, 255, 255, 0.4);
  }

  /* .board-container,
  .info-container {
    float: left;
  } */

  .board-container {
    width: 80%;
  }

  @media (min-width: 1200px) {
    .board-container {
      width: 70%;
      max-width: 600px;
    }
  }

  .info-container {
    padding-top: 20px;
    width: 100%;
  }

  @media (min-width: 1200px) {
    .info-container {
      width: 25%;
      padding-top: 0;
      padding-left: 20px;
    }
  }

  .board {
    border: 1px solid #4abdac;
  }

  .board__row {
    display: -webkit-box;
    display: flex;
  }

  .board__row > .btn {
    -webkit-box-flex: 1;
    flex: 1;
  }

  .board__row > .btn::before {
    content: '';
    display: block;
    padding-top: 100%;
  }

  .x {
    display: block;
    width: 95%;
    height: 95%;
    position: absolute;
    top: 45%;
    left: 50%;
    -webkit-transform: translate(-50%, -40%);
    transform: translate(-50%, -40%);
  }

  .x::before,
  .x::after {
    position: absolute;
    left: 45%;
    content: ' ';
    height: 90%;
    width: 2px;
    background-color: #fc4a1a;
  }

  .x::before {
    -webkit-transform: rotate(45deg);
    transform: rotate(45deg);
  }

  .x::after {
    -webkit-transform: rotate(-45deg);
    transform: rotate(-45deg);
  }

  .o {
    display: block;
    width: 70%;
    height: 70%;
    position: absolute;
    top: 50%;
    left: 50%;
    -webkit-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
    border-radius: 50%;
    border: 2px solid #f7b733;
  }
`;

export default GameWrapper;
