import React from 'react';
import _ from 'lodash';
import Board from './Board';
import GameWrapper from './style';

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      size: 25,
      win: 5,
    };
    this.handleSizeChange = this.handleSizeChange.bind(this);
    this.handleWinChange = this.handleWinChange.bind(this);
  }

  handleSizeChange(e) {
    this.setState({
      size: _.toInteger(e.target.value),
    });
  }

  handleWinChange(e) {
    this.setState({
      win: _.toInteger(e.target.value),
    });
  }

  render() {
    return (
      <GameWrapper>
        <Board size={this.state.size} win={this.state.win} />
      </GameWrapper>
    );
  }
}

export default App;
