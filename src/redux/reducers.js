import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { reducer as notifications } from 'react-notification-system-redux';
import { auth } from './auth/reducer';
import { loading } from './loading/reducer';
import { game } from './game/reducer';

export default combineReducers({
  router: routerReducer,
  auth,
  loading,
  notifications,
  game,
});
