import { takeEvery, put } from 'redux-saga/effects';
import firebase from 'firebase/app';
import { LoginTypes, loginSuccessAction, loginFailAction, logoutSuccessAction } from './actions';
import { loginFBAPI } from '../../api/firebase';

function* getCurrentUserSaga() {
  try {
    const currentUser = yield firebase.auth().currentUser;
    if (currentUser) yield put(loginSuccessAction(currentUser));
  } catch (ex) {
    put(loginFailAction());
  }
}

function* loginSaga() {
  try {
    const data = yield loginFBAPI();
    yield put(loginSuccessAction(data.user));
  } catch (ex) {
    console.error(ex);
  }
}

function* logoutSaga() {
  try {
    yield firebase.auth().signOut();
    yield put(logoutSuccessAction());
  } catch (ex) {
    console.error(ex);
  }
}

export default [
  takeEvery(LoginTypes.LOGIN, loginSaga),
  takeEvery(LoginTypes.GET_CURRENT_USER, getCurrentUserSaga),
  takeEvery(LoginTypes.LOGOUT, logoutSaga),
];
