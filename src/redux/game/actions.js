import { makeConstantCreator, makeActionCreator } from '../reduxCreator';

export const GameTypes = makeConstantCreator('START_GAME', 'UPDATE_GAME_PROCESS');

export const startGameAction = () => makeActionCreator(GameTypes.START_GAME);
export const updateGameProcessAction = data =>
  makeActionCreator(GameTypes.UPDATE_GAME_PROCESS, { data });
