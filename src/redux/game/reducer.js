import _ from 'lodash';
import { GameTypes } from './actions';
import { makeReducerCreator } from '../reduxCreator';
import { X, O } from '../../containers/Game/Square';

const size = 25;

export const initialState = {
  board: _.times(size, () => _.times(size, _.constant(null))),
  current: X,
  prevBoards: [],
  prevCurrent: O,
  won: null,
  winCondition: 5,
};

const startGame = () => {
  return {
    ...initialState,
  };
};

const updateGameProcess = (state, action) => {
  return {
    ...state,
    ...action.data,
  };
};

export const game = makeReducerCreator(initialState, {
  [GameTypes.START_GAME]: startGame,
  [GameTypes.UPDATE_GAME_PROCESS]: updateGameProcess,
});
